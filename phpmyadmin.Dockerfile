# Use an existing image as a base for phpMyAdmin
FROM phpmyadmin/phpmyadmin

# Expose port 80
EXPOSE 80

# Set environment variables for phpMyAdmin
ENV PMA_HOST=mysql_db_server
ENV PMA_PORT=3306
ENV MYSQL_ROOT_PASSWORD=my-secret-pw

# Start phpMyAdmin
CMD ["php", "-S", "0.0.0.0:80", "-t", "/www"]