# Use an existing image as a base for MySQL
FROM mysql:latest

# Set environment variables for MySQL
ENV MYSQL_ROOT_PASSWORD=my-secret-pw

# Start MySQL
CMD ["mysqld"]